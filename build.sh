#!/bin/bash
#============================================================================
#title          :Comets docker build script
#description    :Script to build jupyter docker environment with comets
#author         :Jasper Koehorst
#date           :2021
#version        :0.0.1
#============================================================================

docker login docker-registry.wur.nl

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

#============================================================================
# Build the docker file
#============================================================================

docker build -t docker-registry.wur.nl/unlock/comets .

docker push docker-registry.wur.nl/unlock/comets