import cobra
import cobra.test
import cometspy as c

# Load a textbook example model using the COBRAPy toolbox 
test_model = cobra.test.create_test_model('textbook')

# Use the above model to create a COMETS model
test_model = c.model(test_model)

# Change comets specific parameters, e.g. the initial biomass of the model
# Notre 
test_model.initial_pop = [0, 0, 1e-7] 

# Create a parameters object with default values 
my_params = c.params()

# Change the value of a parameter, for example number of simulation cycles
my_params.set_param('maxCycles', 100)

# Set some writeTotalBiomassLog parameter to True, in order to save the output
my_params.set_param('writeTotalBiomassLog', True)

# See avaliable parameters and their values
print(my_params.show_params())

my_layout = c.layout(test_model)

print(my_layout.media)

my_simulation = c.comets(my_layout, my_params)
try:
    my_simulation.run()
    print(my_simulation.run_output)
    print(my_simulation.run_errors)
except:
    print(my_simulation.run_output)
    print(my_simulation.run_errors)

