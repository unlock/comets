######################################
######################################
# Base Image
FROM jupyter/datascience-notebook

# Metadata
LABEL base.image="Notebook:latest"
LABEL version="1"
LABEL software="Notebook 1.0"
LABEL software.version="1.0.0"
LABEL description="iRods Jupyter Notebook infrastructure"
LABEL website="https://m-unlock.gitlab.io"
LABEL documentation="https://m-unlock.gitlab.io"
LABEL license="NA"
LABEL tags="Notebook"

######################################
### Change to root to install dependencies
USER root

## GUROBI DOCS
RUN apt-get update \
    && apt-get install -y git \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /home/jovyan \
    && rm -rf /var/lib/apt/lists/*
WORKDIR /home/jovyan
RUN git init \
    && git remote add origin https://github.com/Gurobi/modeling-examples.git \
    && git fetch \
    && git checkout -t origin/master -f \
    && rm -rf .git

######################################
## Install gurobi
# FROM ubuntu:20.04 as buildoptimizer
ARG GRB_VERSION=9.1.2
ARG GRB_SHORT_VERSION=9.1

# install gurobi package and copy the files
WORKDIR /opt

RUN apt-get update \
    && apt-get install --no-install-recommends -y\
       ca-certificates  \
       wget \
    && update-ca-certificates \
    && wget -v https://packages.gurobi.com/${GRB_SHORT_VERSION}/gurobi${GRB_VERSION}_linux64.tar.gz \
    && tar -xvf gurobi${GRB_VERSION}_linux64.tar.gz  \
    && rm -f gurobi${GRB_VERSION}_linux64.tar.gz \
    && mv -f gurobi* gurobi \
    && rm -rf gurobi/linux64/docs


# After the file renaming, a clean image is build
# FROM python:3.7-slim AS packageoptimizer

ARG GRB_VERSION=9.1.2

LABEL vendor="Gurobi"
LABEL version=${GRB_VERSION}

# update system and certificates
RUN apt-get update \
    && apt-get install --no-install-recommends -y\
       ca-certificates  \
       p7zip-full \
       zip \
    && update-ca-certificates \
    && rm -rf /var/lib/apt/lists/*

# WORKDIR /opt/gurobi
# COPY --from=buildoptimizer /opt/gurobi .

ENV GUROBI_HOME /opt/gurobi/linux64
ENV PATH $PATH:$GUROBI_HOME/bin
ENV LD_LIBRARY_PATH $GUROBI_HOME/lib

WORKDIR /opt/gurobi/linux64
#run the setup
RUN python setup.py install

######################################
## Install comets
USER root
COPY --chown=${NB_UID}:${NB_GID} comets_2.10.5_linux.tar.gz /tmp/
RUN tar -xvf /tmp/comets_2.10.5_linux.tar.gz -C /bin/ && \
    chmod -R 777 /bin/comets_2.10.5_linux/


######################################


WORKDIR /
USER jovyan

### Change to root to install dependencies
USER root

RUN apt-get update && apt-get install -y build-essential g++ python3-pybind11

USER jovyan

## All python packages
### Latest version of irods client only available via pip install
COPY --chown=${NB_UID}:${NB_GID} requirements.txt /tmp/
RUN pip install --no-cache-dir --requirement /tmp/requirements.txt && \
    fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}"

# RUN pip install python-irodsclient distance python-irodsclient SPARQLWrapper importlib pybind11 rdflib-hdt

### Jupyter extensions under the right user
RUN pip install jupyter_contrib_nbextensions
RUN jupyter contrib nbextension install --user
RUN jupyter nbextensions_configurator enable --user
# RUN jupyter labextension install @wallneradam/output_auto_scroll

# docker build -t docker-registry.wur.nl/unlock/notebook:jupyter . 
# docker run -it --rm -p 8888:8888 docker-registry.wur.nl/unlock/notebook:jupyter start.sh jupyter lab


### JAVA
USER root
RUN apt-get install -y default-jre
USER jovyan
